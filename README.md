* Setup Virtualenv
* Install dependencies: `pip install -r requirements.txt`
* Start Vagrant box `vagrant up`
* Setup wordpress `ansible-playbook -i hosts setup-wordpress.yml`
* Setup netdata monitoring `ansible-playbook -i hosts setup-netdata.yml`
* WP: `http://127.0.0.1`
* Netdata: `http://127.0.0.1:19999`